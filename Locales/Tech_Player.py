import random


class Tech_Player:
    def __init__(self, kind=False, name='', aka=False):
        # Kind = 1 if IA
        if kind is True:
            self.__kind = True
            self.__aka = 'El mejor jugador'
        elif kind is False:
            if aka:
                self.__aka = aka
            else:
                self.__aka = False
        if name != 'player1' and name != 'player2':
            raise ValueError
            # print('NO PUEDES INSTANCIR UN JUGADOR SIN UN NOMBRE APROPIADO')
            self.status = False
        else:
            self.__name = name
            self.status = True
        self.__apodos_random = {
            1: 'Razen 5600x',
            2: 'Conejito malo',
            3: 'Computadora Caliente',
            4: 'Puerta rechinante',
            5: 'Cámara de gas',
            6: 'Ventilador volador',
        }

    @property
    def Name(self):
        return self.__name

    @property
    def Apodo(self):
        if self.__aka is not False:
            return self.__aka
        elif self.__aka is False:
            x = random.randint(1, 6)
            x = self.__apodos_random[x]
            self.__aka = x
            return self.__aka
