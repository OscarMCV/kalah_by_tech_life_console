

def Graficar_Tablero(game=None):
    if game is None:
        f = open('/home/oscarmcv/Documents/kalah_by_tech_life_console/Media/Tablero', 'r')
        response = f.read()
        response = response.format(
            CELL00='00',
            CELL01='00',
            CELL02='00',
            CELL03='00',
            CELL04='00',
            CELL05='00',
            CELL10='00',
            CELL11='00',
            CELL12='00',
            CELL13='00',
            CELL14='00',
            CELL15='00',
            PLAYER1='00',
            PLAYER2='00',
        )
        return response
    else:
        board = game.get_board()
        kalah_player1 = board.Kalah(game.Player1)
        kalah_player2 = board.Kalah(game.Player2)
        f = open('/home/oscarmcv/Documents/kalah_by_tech_life_console/Media/Tablero (copy)', 'r')
        response = f.read()
        rango = range(len(kalah_player1))
        for index in rango:
            if (kalah_player1[index] / 10) < 1:
                kalah_player1[index] = '0' + str(kalah_player1[index])
        for index in range(len(kalah_player2)):
            if (kalah_player2[index] / 10) < 1:
                kalah_player2[index] = '0' + str(kalah_player2[index])
        jugador_en_turno = game.get_player_in_turn()
        response = response.format(
            # Lista del jugador 1
            CELL00=kalah_player1[1],
            CELL01=kalah_player1[2],
            CELL02=kalah_player1[3],
            CELL03=kalah_player1[4],
            CELL04=kalah_player1[5],
            CELL05=kalah_player1[6],
            # Lista del jugador 2
            CELL10=kalah_player2[1],
            CELL11=kalah_player2[2],
            CELL12=kalah_player2[3],
            CELL13=kalah_player2[4],
            CELL14=kalah_player2[5],
            CELL15=kalah_player2[6],
            PLAYER1=kalah_player1[0],
            PLAYER2=kalah_player2[0],
            NOMBRE_JUGADOR=jugador_en_turno.Apodo,
            PLAYERNAME=jugador_en_turno.Name,
        )
        return response
