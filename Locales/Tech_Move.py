class Tech_Move:
    def __init__(self, posicion):
        if posicion > 0 and posicion < 7:
            self.__movimiento = posicion
        else:
            raise ValueError

    @property
    def Posicion(self):
        return self.__movimiento
