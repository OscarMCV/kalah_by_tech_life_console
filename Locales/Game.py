from Clases_herencia import minimax
from Clases_herencia.game import Game
from Locales.Tech_Player import Tech_Player
from Locales.Tech_Board import Tech_Board
from Clases_herencia.minimax import Minimax


class Tech_Game(Game):

    """
    Por defecto, la clase asume que el usuario quiere iniciar
    un juego contra la inteligencia.
    Cuando se juega contra la iteligencia, esta siempre
    será Kind = true,
    Parámetros de Game:
    player1,
    player2,
    board,
    """

    def __init__(self, IA=False, player1=None, player2=None):
        # mode True si se jugara contra la inteligencia
        if IA:
            # Player 1 Es IA
            self.__player1 = Tech_Player(
                name='player1',
                kind=True
            )
            # Player dos es humano
            self.__player2 = Tech_Player(
                name='player2',
                aka=player2
            )
        else:
            # PLayer 1 es humano
            self.__player1 = Tech_Player(
                name='player1',
                aka=player1
            )
            # PLayer 2 es humano
            self.__player2 = Tech_Player(
                name='player2',
                aka=player2
            )
        self.__board = Tech_Board()

    @property
    def Player1(self):
        return self.__player1

    @property
    def Player2(self):
        return self.__player2

    def get_board(self):
        # Devuelve objeto tablero
        return self.__board

    def get_player_in_turn(self):
        # Devuelve objeto player
        if self.__board.Jugador() == 'player1':
            return self.__player1
        else:
            return self.__player2

    def get_opponent(self):
        if self.__board.Jugador() == 'player1':
            return self.__player2
        else:
            return self.__player1

    def check_ended(self):
        # Devuelve booleano
        return self.__board.check_ended()

    def get_winner(self):
        # Devuelve el resultado del tablero
        if self.__board.check_winner(
            player=self.__player1
        ):
            return self.__player1
        if self.__board.check_winner(
            player=self.__player2
        ):
            return self.__player2
        else:
            return False

    def change_turn(self):
        self.__board.Alternar()

    def make_human_move(self, move):
        self.__board = self.__board.make_move(move=move, player=self.get_player_in_turn())

    def make_ai_move(self):
        board = self.__board.copy()
        # Se crea una copia para que el algoritmo no manipule
        # directamente el tablero local
        print('El jugador es desde el board', board.Jugador())
        player = self.get_player_in_turn()
        print('El apodo, desde el player es', player.Apodo)
        opponet = self.get_opponent()
        print('El apodo, desde el opponent es', opponet.Apodo)
        ai_move = Minimax.solve(
            board=board,
            player=player,
            opponent=opponet,
        )
        self.__board = self.__board.make_move(
            move=ai_move[1],
            player=self.__player1,
        )
