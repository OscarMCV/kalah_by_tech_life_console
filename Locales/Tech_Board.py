#  from Move import Tech_Move
from Locales.Tech_Move import Tech_Move
from Clases_herencia import board
import random
# import numpy as np


class Tech_Board(board.Board):
    def __init__(self, kalah0=None, kalah1=None, jugador_en_turno=None):
        if kalah0 is None and kalah1 is None and jugador_en_turno is None:
            # Inicia una lista de lista que representan las 12 kalahas del juego
            rand_dict = {1: 'player1', 2: 'player2'}
            self.__turno = rand_dict[random.randint(1, 2)]
            self.__kalah = []
            self.__kalah.append([])
            self.__kalah.append([])
            for index in range(14):
                if index != 0 and index != 7:
                    self.__kalah[0].append(5) if index % 2 == 0 else self.__kalah[1].append(5)
                elif index == 7:
                    self.__kalah[1].insert(0, 0)
                elif index == 0:
                    self.__kalah[0].insert(0, 0)
        elif jugador_en_turno != 'player1' and jugador_en_turno != 'player2':
            raise ValueError
        else:
            self.__kalah = []
            self.__kalah.append(kalah0)
            self.__kalah.append(kalah1)
            # self.__jugador en turno es un string, no un objeto player
            self.__turno = jugador_en_turno

    def Kalah(self, player):
        if player.Name == 'player1':
            return self.__kalah[0].copy()
        elif player.Name == 'player2':
            return self.__kalah[1].copy()
        else:
            raise ValueError

    def Alternar(self):
        if self.__turno == 'player1':
            self.__turno = 'player2'
        else:
            self.__turno = 'player1'

    def Jugador(self):
        return self.__turno

    def Tablero(self, player):
        if player.Name == 'player1':
            return self.__kalah[0][0]
        elif player.Name == 'player2':
            return self.__kalah[1][0]

    def calculate_score(self, player, opponent):
        # Recibe jugador en turno y oponente, devuelve un entero equivalente al score
        score = self.Tablero(player) - self.Tablero(opponent)
        return score

    def check_ended(self):
        # Devuelve verdadero o falso para determinar si el juego ya se ha terminado
        check_player1 = [True for x in self.__kalah[0] if x > 0]
        check_player2 = [True for x in self.__kalah[1] if x > 0]
        if True in check_player1 and True in check_player2:
            return False
        else:
            return True

    def get_valid_moves(self, player=None, movement=None):
        # Devuelve una lista de las posibles jugadas del jugador ingresado
        if player.status is True and movement is None:
            kalah = self.Kalah(player)
            moves = [Tech_Move(x) for x in range(1, 7) if kalah[x] > 0]
            return moves
        # Regresa verdadero si el movimiento es válido para el jugador
        if movement and player.status is True:
            # Crea un a copia de la lista local
            kalah = self.Kalah(player)
            # Crea una lista de movimientos válidos para el jugador dado
            moves = [Tech_Move(x) for x in range(1, 7) if kalah[x] > 0]
            # Genera un True en la lista si el movimiento existe en la lista
            # de movimientos válidos
            _moves = [True for x in range(len(moves)) if moves[x].Posicion == movement.Posicion]
            if True in _moves:
                return True
            else:
                return False
        else:
            raise ValueError

    def make_move(self, player, move, inplace=True):
        # Recibe jugador en turno, objeto Move, regresa un tablero
        # Si el movimiento es válido
        # print(self.__turno, ' ', player.Name)
        if self.get_valid_moves(player=player, movement=move):
            # Kalah1 representa el tablero del jufador1
            kalah1 = self.__kalah[0].copy()

            # Kalah1 representa el tablero del jufador2
            kalah2 = self.__kalah[1].copy()

            rango2 = move.Posicion
            if player.Name == 'player1':
                rango = kalah1[move.Posicion]
                kalah1[move.Posicion] = 0
                for index in reversed(range(1, rango + 1)):
                    #================================
                    #  Superior hacia la izquierda (propio)
                    #================================
                    if rango2 > 0 and rango2 < 8:
                        # Range tiene que estar entre 1 y 6
                        rango2 -= 1
                        kalah1[rango2] += 1
                        # Reaccionar a reiniciar el ciclo en tablero propio
                        # sin darle puntos al oponente
                        
                        # Si en el último ciclo cayó en talbero propio pero
                        # no estaba vacío, cambio de turno
                        if index == 1 and rango2 != 1:
                            self.Alternar()
                            
                        if rango2 == 0 and index != 1:
                            rango2 = -7
                    elif rango2 > -8 and rango2 < -1:
                        #================================
                        #  Inferior hacia la derecha (ajeno)
                        #================================
                        # Range tiene que estar entre -7 y -1
                        rango2 += 1
                        kalah2[rango2] += 1
                        # Reaccionar a reiniciar el ciclo en tablero Ajeno
                        if rango2 == 0 and index != 1:
                            rango2 = 7
                        if index == 1:
                            # Como es tablero ajeno, se alterna
                            print('alternó AQUÏ')
                            self.Alternar()
            elif player.Name == 'player2':
                rango = kalah2[move.Posicion]
                kalah2[move.Posicion] = 0
                traductor = {6: -1, 5: -2, 4: -3, 3: -4, 2: -5, 1: -6}
                rango2 = traductor[rango2]
                for index in reversed(range(1, rango + 1)):
                    #================================
                    #  Superior hacia la izquierda (ajeno)
                    #================================
                    if rango2 == 1:
                        rango2 = -6

                    if rango2 > 0 and rango2 < 8:
                        # Range tiene que estar entre 1 y 6
                        rango2 -= 1
                        kalah1[rango2] += 1
                        # Reaccionar a reiniciar el ciclo en tablero propio
                        # sin darle puntos al oponente
                        if rango2 == 0 and index != 1:
                            rango2 = -7
                        # Si en el último ciclo cayó en talbero propio pero
                        # no estaba vacío, cambio de turno
                        if index == 1:
                            self.Alternar()
                    elif rango2 > -8 and rango2 <= -1:
                        #================================
                        #  Inferior hacia la derecha (propio)
                        #================================
                        # Range tiene que estar entre -7 y -1
                        rango2 += 1
                        kalah2[rango2] += 1
                        # Reaccionar a reiniciar el ciclo en tablero Ajeno
                        if index == 1 and rango2 != 0:
                            if index == 1 and kalah2[rango2] != 0:
                                # Como es tablero ajeno, se alterna
                                self.Alternar()
                        if rango2 == 0 and index != 1:
                            rango2 = 7
 
            return Tech_Board(kalah0=kalah1, kalah1=kalah2, jugador_en_turno=self.Jugador())

    def check_winner(self, player):
        # Recibe un jugador y dice si va ganando o no
        temp = self.Kalah(player)
        if temp[0] > 30:
            return True

    def copy(self,):
        response = Tech_Board(
            kalah0=self.__kalah[0],
            kalah1=self.__kalah[1],
            jugador_en_turno=self.__turno,
        )
        return response
