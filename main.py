
#%%
from Locales.Tech_Move import Tech_Move
from Locales.Graficar_Tablero import Graficar_Tablero
from Locales.Game import Tech_Game
import os
clear = lambda: os.system('clear')


def loop_juego_para_IA(game):
    red_button = False
    Current_Game = game
    while red_button is False:
        if Current_Game.get_player_in_turn().Name == 'player1':
            print('Tablero \n', Graficar_Tablero(Current_Game))
            print('Es mi turno, presiona una tecla para ver mi jugada')
            str(input())
            print('Estoy pensando....')
            Current_Game.make_ai_move()
            clear()
        elif Current_Game.get_player_in_turn().Name == 'player2':
            print('Tablero \n', Graficar_Tablero(Current_Game))
            print('\n Cuál es tu posición? (1/6)')
            posicion = Tech_Move(int(input()))
            Current_Game.make_human_move(move=posicion)
            clear()
        red_button is True if Current_Game.check_ended() is False else red_button is False


def loop_juego(game):
    Current_Game = game
    print('Jugador1, te llamaré', Current_Game.Player1.Apodo)
    print('Jugador2, te llamaré', Current_Game.Player2.Apodo)
    red_button = False
    
    while red_button is False:
        if Current_Game.get_player_in_turn().Name == 'player1':
            print('Tablero \n', Graficar_Tablero(Current_Game))
            print('\n Cuál es tu posición? (1/6)')
            posicion = Tech_Move(int(input()))
            Current_Game.make_human_move(move=posicion)
            clear()
        elif Current_Game.get_player_in_turn().Name == 'player2':
            print('Tablero \n', Graficar_Tablero(Current_Game))
            print('\n Cuál es tu posición? (1/6)')
            posicion = Tech_Move(int(input()))
            Current_Game.make_human_move(move=posicion)
            clear()
        red_button is True if Current_Game.check_ended() is False else red_button is False


def main():
    clear()
    response_yes = 'y'
    response_no = 'n'
    status = False
    modes = {'1': True, '2': False}
    Bienvenida = "                          Bienvenido al juego \n \n"
    print(Bienvenida, Graficar_Tablero(), '\n Presiona 1 si quieres jugar contra mí, o 2 si quieres jugar con un amigo (1/2) \n')
    modo = str(input())
    if modo == '1' or modo == '2':
        modo = modes[modo]
        # Reaccionando a jugar contra la computadora
        if modo:
            while status == False:
                print('Quieres darme tu nombre? (y/n)')
                tienenombre = str(input())
                if tienenombre == response_yes:
                    print('Cómo te llamas?')
                    nombre1 = str(input())
                    Current_Game = Tech_Game(IA=modo, player2=nombre1)
                    print('Excelente!! ', Current_Game.Player2.Apodo, 'que gane el mejor, mucha suerte, la necesitarás :)')
                    # Inicia el jeugo con el modo especificado
                    Current_Game = Tech_Game(IA=modo, player2=nombre1)
                    status = True
                elif tienenombre == response_no:
                    Current_Game = Tech_Game(modo)
                    clear()
                    print('De acuardo, te apodaré: ', Current_Game.Player2.Apodo)
                    print('No lo tomes a mal, puedes llamarme: ', Current_Game.Player1.Apodo)
                    status = True
                else:
                    print('Respuesta inválida, inicia de nuevo')
            print('Que comience el juego!!')
            loop_juego_para_IA(Current_Game)
        else:
            clear()
            print('Jugador1, quieres dárme tu nombre? (y/n)')
            tienenombre = str(input())
            if tienenombre == 'y':
                print('Qué amable!, Cómo te llamas?')
                nombre1 = str(input())
            elif tienenombre == 'n':
                print('hmm, ok, yo me las areglaré')
                nombre1 = None
            print('Jugador2, quiéres dárme tu nombre? (y/n)')
            tienenombre2 = str(input())
            if tienenombre2 == 'y':
                print('Qué amable!, Cómo te llamas?')
                nombre2 = str(input())
            elif tienenombre2 == 'n':
                print('Lamento oir... interpretar eso')
                nombre2 = None
            Current_Game = Tech_Game(player1=nombre1, player2=nombre2)
            print('Que comience el juego, comienza el jugador1')
            loop_juego(Current_Game)


main()
        # %%
