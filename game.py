import abc
from Python.player import Player
from Python.board import Board



class Game(abc.ABC):
    """
    Por defecto, la clase asume que el usuario quiere iniciar un juego contra la inteligencia.
    Cuando se juega contra la iteligencia, esta siempre será Kind = true,
    Parámetros de Game:
    player1,
    player2,
    board,

    """

    def __init__(self, IA=True):
        # mode True si se jugara contra la inteligencia
        if IA:
            # Player 1 Es IA
            self.__player1 = Player(
                name='Player1',
                kind=True
            )
            # Player dos es humano
            self.__player2 = Player(
                name='Player2',
            )
        else:
            # PLayer 1 es humano
            self.__player1 = Player(
                name='Player1',
            )
            # PLayer 2 es humano
            self.__player2 = Player(
                name='Player2'
            )
        self.__board = Board()
        

    @abc.abstractmethod
    def get_board(self):
        # Devuelve objeto tablero

    @abc.abstractmethod
    def get_player_in_turn(self):
        # Devuelve objeto player



    @abc.abstractmethod
    def check_ended(self):
        # Devuelve booleano
        return self.__board.


    @abc.abstractmethod
    def get_winner(self):
        # Devuelve el resultado del tablero

    @abc.abstractmethod
    def change_turn(self):


    @abc.abstractmethod
    def make_human_move(self, move):

    @abc.abstractmethod
    def make_ai_move(self):
    
