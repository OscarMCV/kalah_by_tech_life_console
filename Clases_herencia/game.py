import abc


class Game(abc.ABC):

    @abc.abstractmethod
    def get_board(self):
        pass

    @abc.abstractmethod
    def get_player_in_turn(self):
        pass

    @abc.abstractmethod
    def check_ended(self):
        pass

    @abc.abstractmethod
    def get_winner(self):
        pass

    @abc.abstractmethod
    def change_turn(self):
        pass

    @abc.abstractmethod
    def make_human_move(self, move):
        pass

    @abc.abstractmethod
    def make_ai_move(self):
        pass
