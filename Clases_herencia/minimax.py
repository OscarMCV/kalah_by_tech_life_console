class Minimax:

    @staticmethod
    def solve(board, player, opponent, levels=3, maximizing=True,
              alpha=float('-inf'), beta=float('inf'), move=None):
        result = None
        if levels == 0 or board.check_ended():
            value = board.calculate_score(player, opponent) * \
                (1 if maximizing else -1)
            result = (value, move)
        elif maximizing:
            best = (float('-inf'), None)
            for move_to_analyze in board.get_valid_moves(player):
                value = Minimax.solve(
                    board.make_move(player, move_to_analyze, inplace=False),
                    opponent, player, levels - 1, not maximizing, alpha, beta,
                    move_to_analyze)
                if value[0] > best[0]:
                    best = (value[0], move_to_analyze)
                alpha = max(alpha, best[0])
                if alpha >= beta:
                    break
            result = best
        else:
            best = (float('inf'), None)
            for move_to_analyze in board.get_valid_moves(player):
                value = Minimax.solve(
                    board.make_move(player, move_to_analyze, inplace=False),
                    opponent, player, levels - 1, not maximizing, alpha, beta,
                    move_to_analyze)
                if value[0] < best[0]:
                    best = (value[0], move_to_analyze)
                beta = min(beta, best[0])
                if beta <= alpha:
                    break
            result = best
        return result
