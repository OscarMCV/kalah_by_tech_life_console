import abc


class Board(abc.ABC):
    @abc.abstractmethod
    def calculate_score(self, player, opponent):
        pass

    @abc.abstractmethod
    def check_ended(self):
        pass

    @abc.abstractmethod
    def get_valid_moves(self, player):
        pass

    @abc.abstractmethod
    def make_move(self, player, move, inplace=True):
        pass

    @abc.abstractmethod
    def check_winner(self, player):
        pass
